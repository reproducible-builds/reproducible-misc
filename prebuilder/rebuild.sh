#!/bin/bash

SCRIPT_PATH="$(realpath $(which "$0"))"
SCRIPT_DIR="$(dirname $SCRIPT_PATH)"
MYNAME=`basename -- "$0"`

[ -f "$SCRIPT_DIR/config" ] && . "$SCRIPT_DIR/config"

BASEPATH="${BASEPATH:-/var/cache/pbuilder/base-reproducible.tgz}"
USE_COWBUILDER=
NUM_CPU="$(grep -c '^processor' /proc/cpuinfo)"
ARCH="$(dpkg --print-architecture)"

usage() {
    echo "$MYNAME: build a package twice with variations"
    echo ""
    echo "Synopsis:"
    echo "    $MYNAME [-b BASETGZ] [-c] [-h] PACKAGENAME"
    echo ""
    echo "Options:"
    echo "  -b BASETGZ    Specify path to pbuilder's --basetgz."
    echo "  -c            Use cowbuilder."
    echo "  -h            Show this usage message."
    echo "  PACKAGENAME   Name of a *.dsc source package file."
    echo "                The name may be given up to underscore, or in full."
    echo "                The file must exist in the current directory."
}

while getopts b:ch o; do
	case $o in
	b )	BASEPATH="$OPTARG"
		[ "${BASEPATH%.cow}" != "${BASEPATH}" ] && USE_COWBUILDER=true
		;;
	c )	USE_COWBUILDER=true;;
	h )	usage; exit 0;;
	esac
done
shift "$(expr $OPTIND - 1)"

if [ -n "$USE_COWBUILDER" ]; then
	BUILDER=cowbuilder
	BUILDER_OPTS="--basepath $BASEPATH"
else
	BUILDER=pbuilder
	BUILDER_OPTS="--basetgz $BASEPATH"
fi

if [ $# -ne 1 ]; then
    usage >&2
    exit 1
fi

PACKAGE="$1"
PACKAGE=${PACKAGE#./}
PACKAGE=${PACKAGE%_*}
case ${PACKAGE} in
  */*) echo >&2 "$MYNAME: error: Package file must be in the current directory."; exit 1;;
esac

rm -rf b1 b2
mkdir -p b1 b2 logs

sudo -E \
 DEB_BUILD_OPTIONS="parallel=$((NUM_CPU+1)) nocheck" \
 unshare --uts -- $BUILDER --build $BUILDER_OPTS \
    --configfile "$SCRIPT_DIR/pbuilderrc.build" \
    --debbuildopts "-b" \
    --buildresult b1 \
    --logfile logs/${PACKAGE}.build1 \
    ${PACKAGE}_*.dsc
xz < logs/${PACKAGE}.build1 > logs/${PACKAGE}.build1.xz
rm -f logs/${PACKAGE}.build1

# Let's make a tarball of the build result. This allows us
# to give it to the second run of pbuilder. The configured
# hook will then run diffoscope at the end of the second
# build.

TMPDIR=$(mktemp -d) || exit 1
trap "rm -rf '$TMPDIR'" EXIT
TMPTAR="$TMPDIR/initial_build.tar"

(cd b1 && tar -cf "$TMPTAR" $(dcmd ${PACKAGE}_*.changes))

sudo -E \
 DEB_BUILD_OPTIONS="parallel=$NUM_CPU nocheck" \
 linux64 --uname-2.6 unshare --uts -- $BUILDER --build $BUILDER_OPTS \
    --configfile "$SCRIPT_DIR/pbuilderrc.rebuild" \
    --debbuildopts "-b" \
    --buildresult b2 \
    --logfile logs/${PACKAGE}.build2 \
    --hookdir "$SCRIPT_DIR/pbuilderhooks" \
    --inputfile "$TMPTAR" ${PACKAGE}_*.dsc
xz < logs/${PACKAGE}.build2 > logs/${PACKAGE}.build2.xz
rm -f logs/${PACKAGE}.build2

[ -f b2/diffoscope.html ] && mv b2/diffoscope.html logs/${PACKAGE}.diffoscope.html
[ -f b2/diffoscope.txt ] && {
	xz < b2/diffoscope.txt > logs/${PACKAGE}.diffoscope.txt.xz
	rm -f b2/diffoscope.txt
}
