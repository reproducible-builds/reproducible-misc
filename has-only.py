#!/usr/bin/python3

import itertools
import json
import re
import sys
from typing import Iterator, Tuple, Optional, Set

DO_FILE_PREFIX = '├── '
DO_DIFF_PREFIX = re.compile('(?:│ )+([+-])(.*)')
XXD_LINE = re.compile('[0-9a-f]{8}: (?:[0-9a-f]{4} ){8} (.{16})')
XXD_END = re.compile('[0-9a-f]{8}: [0-9a-f]{2}(?:[0-9a-f]{2}|  ) (?:[0-9a-f ]{4} ){7} (.{1,16})')

PACKAGE_FROM_PATH = re.compile('.*/([a-z0-9.+-]+)_[^/]+')

TOO_BIG = 1024 * 50


def diff_parse(lines):
    file = None
    in_add = False
    replacing = []
    replacement = []

    for line in itertools.chain(lines, ['\n']):
        if line.startswith(DO_FILE_PREFIX):
            file = line[len(DO_FILE_PREFIX):].strip()
            replacing = []
            replacement = []
            continue
        ma = DO_DIFF_PREFIX.match(line)
        if ma:
            adding = ma.group(1) == '+'
            data = ma.group(2)
            if not adding:
                if replacement:
                    yield (file, replacing, replacement)
                    replacing = []
                    replacement = []
                replacing.append(data)
            else:
                replacement.append(data)
            continue

        if replacing or replacement:
            yield (file, replacing, replacement)
            replacing = []
            replacement = []


def strip_xxd(lines):
    stripped = ''
    for no, line in enumerate(lines):
        ma = XXD_LINE.match(line)
        if not ma:
            if no == len(lines) - 1:
                end = XXD_END.match(line)
                if end:
                    stripped += end.group(1)
                    break
            return '\n'.join(lines)
        stripped += ma.group(1)

    return stripped


def process(f):
    has = set()
    hunks = diff_parse(f)
    without_xxd = ((file, strip_xxd(bef), strip_xxd(aft)) for file, bef, aft in hunks)
    for file, before, after in without_xxd:
        has = set()

        if file in {
            'file list',
            'md5sums',
            'Files',
            './control',
            'line order',
            'zipinfo {}',
            'zipinfo -v {}',
            }:
            # TODO: this is a massive approximation; ignores variations like deleting a file
            continue

        if '1st/' in before and '/2nd' in after:
            has.add('build_path')

        if 'pdftk {} output - uncompress' == file and '/ID [<' in before and '/ID [<' in after:
            has.add('pdf')

        if 'Build ID: ' in before and 'Build ID: ' in after:
            has.add('build_id')

        if 'readelf --wide --debug-dump=info {}' == file:
            has.add('build_id')

        if file.startswith('readelf ') and '.debug' in before and '.debug' in after:
            has.add('build_id')

        if file.endswith('Makefile') and 'SHELL = /bin/' in before and 'SHELL = /bin/' in after:
            has.add('auto_shell')

        if file.endswith('.rdb') or file.endswith('.rdx') or file.endswith('.rds') \
                or file.endswith('.rdx-content'):
            has.add('rdb')

        if len(before) > TOO_BIG or len(after) > TOO_BIG:
            has.add('huge')

        yield (file, has, before, after)
        # if not has:
        #    raise Exception("unclassified: {}".format((file, before, after)))


def warn(path, msg):
    sys.stderr.write('{}: {}\n'.format(path, msg))


def suggest_notes(file_problems: Iterator[Tuple[str, Tuple[str, ...]]]) -> Optional[Set[str]]:
    problems = set(tuple(sorted(probs)) for _, probs, _, _ in file_problems)

    if 1 == len(problems) and ('build_id',) in problems:
        return {'build_id_differences_only'}

    notes = set()
    distinct_labels = set(x for y in problems for x in y)
    if 'build_path' in distinct_labels:
        notes.add('captures_build_path')

    if 'auto_shell' in distinct_labels:
        notes.add('captures_shell_variable_in_autofoo_script')

    return notes


def find_interesting(file_problems: Iterator) -> Optional[Tuple]:  # or something
    for file, problems, before, after in file_problems:
        if not problems:
            return file, before, after

    return None


def package_from(path: str) -> str:
    return PACKAGE_FROM_PATH.match(path).group(1)


def main():
    int_diffs = []
    suggested_notes = {}
    for path in sys.argv[1:]:
        try:
            with open(path) as f:
                file_problems = list(process(f))

            interesting = find_interesting(file_problems)
            if interesting:
                int_diffs.append([path] + list(interesting))

            notes = suggest_notes(file_problems)
            if notes:
                suggested_notes[package_from(path)] = notes

        except Exception as e:
            warn(path, 'failure:')
            raise

    with open('intdiffs.json', 'w') as f:
        json.dump(int_diffs, f)

    with open('suggested_notes.yml', 'w') as f:
        for pkg in sorted(suggested_notes.keys()):
            f.write(pkg)
            f.write(':\n  issues:\n')
            for note in suggested_notes[pkg]:
                f.write('    - ')
                f.write(note)
                f.write('\n')


if '__main__' == __name__:
    main()
